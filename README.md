Navon task
===============
*Semester work from Cognitive systems subject*

# General description
[Navon task](https://www.psytoolkit.org/experiment-library/navon.html) is
David Navon’s paper about the speed with which people process global and 
information is extremely popular (Navon, 1977).

The basic idea of [Navon’s study](https://en.wikipedia.org/wiki/Navon_figure) is that when objects are arranged in groups, 
there are global features and local features. For example, a group of trees 
has local features (the individual trees) and the feature of a forest 
(the trees together).

The basic finding of Navon’s work is that people are faster in 
identifying features at the global than at the local level. 
This effect is also known as global precedence.

An example of a Navon figure is shown below. The figure has a global feature, 
it looks like an T. Its local feature are the many small letters S 
the figure is made of. People are typically quicker detecting an T than an S.

```
SSSSSSSSSSSSSSSSSSSSSSSSS
SSSSSSSSSSSSSSSSSSSSSSSSS
SS        SSSSS        SS
          SSSSS
          SSSSS
          SSSSS
          SSSSS
          SSSSS
          SSSSS
         SSSSSSS
      SSSSSSSSSSSSS
```

# Implementation
This implementation is written in Python+PyQT5
Main features:
* Configuration possibilities
* Minimalistic design : )
* Results are written in a log file

## How to run
* Install libraries from `./environment.yml`
    * You can use conda with:
  ```
  conda env create -f environment.yml
  conda activate ksy-navon
  ```
    * Full list of the current dependencies is below
    
* Run `python ./main.py`

## Task
In this task you need to press the key b on your keyboard or button "B" under the task, \
if you see sny of the given target letters (e.g. "H" and "O").
Otherwise press "N" button (standing for "no").

These letters may appear as a part of a bigger letter like here:
```
OOOOOOO
O
O
OOOOOOO
O
O
OOOOOOO
```
Or be consisted of smaller letters like here:
```
J     J
J     J
J     J
JJJJJJJ 
J     J
J     J
J     J
```
In the both cases you need to press "B". Otherwise press "N", like in the example below:

```
GGGGGGG
G
G
GGGGGGG
G
G
G

K
K
K
K
K
K
KKKKKKK
```
You will see defined number of such random letters for some steps in a row.
You will have a few seconds to consider if you see the target letters in some form or not, 
and press a proper button under the letters or on your keyboard.  

**If the task is configured, to show many complex letters per a step, 
only one of the letter of them may contain/be a target letter (with 50% chance, 25% to be and 25% to contain)** 

After the test you will see the statistics in gui. 
Also, they will be written to the logfile `./log.txt` with a current date and time

The instructions appear at the start of the application.

# Configuration 
The config is written in yaml `config.yaml` \
The possibilities for configuration:
* How many tasks will user obtain at the test (30 by default)
* How many complex letters has each task (1 by default)
* The target letters and the whole list of the used letters (non-target and target). All of the English alphabet's letters may be used
* Intervals between the letters in px. 
  * Different graphical intervals may influence the results of the task
* Time given for a one task step completion.
  * Time pressure can influence the results
* Time after a task step completion (delay before a new step)

Also, custom letters and symbols may be added to the `codes.py`. Be sure you add them to the `codes` dict variable with their one-symbol representation.

# Possible improvements
* Configuration via GUI
* Not so minimalistic GUI
* Better statistics
    * Comparing with the others results - complex with different configurations
    * Showing of some bias with default config 
* Better time computation (current one gives a bit bigger numbers, then should)
* Perform well-defined usability testing and gather more  