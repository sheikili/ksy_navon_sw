# tuples of x and y for a "pixels", (float or int, float or int)
A_code = [(3, 0), (2.5, 1), (3.5, 1), (2, 2), (4, 2), (1.5, 3), (4.5, 3),
          (1, 4), (5, 4), (0.5, 5), (5.5, 5), (0, 6), (6, 6),
          (2, 4), (3, 4), (4, 4)]

B_code = [(0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5), (0, 6),  # Vertical line at 0x
          (1, 0), (2, 0), (3, 0),  # Short Horizontal line at 0y
          (1, 3), (2, 3), (3, 3),  # Short Horizontal line at 3y
          (1, 6), (2, 6), (3, 6),  # Short Horizontal line at 6y
          (4, 1), (4, 2), (4, 4), (4, 5)]

C_code = [(0, 3), (0, 2), (0, 4), (0.3, 1), (0.3, 5), (1.1, 0.1), (1.1, 5.9), (2.1, 0), (2.1, 6),
          (3, 0), (3, 6), (4, 0.2), (4, 5.8), (5, 1), (5, 5)]

D_code = [(6, 3), (6, 2), (6, 4), (5.7, 1), (5.7, 5), (4.9, 0.1), (4.8, 5.9), (3.9, 0), (3.9, 6),
          (3, 0), (3, 6), (2, 0), (2, 6), (1, 0), (1, 6),
          (0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5), (0, 6)]  # Vertical line at 0x

E_code = [(0, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0),  # Horizontal line at 0y
          (0, 3), (1, 3), (2, 3), (3, 3), (4, 3), (5, 3), (6, 3),  # Horizontal line at 3y
          (0, 6), (1, 6), (2, 6), (3, 6), (4, 6), (5, 6), (6, 6),  # Horizontal line at 6y
          (0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5), (0, 6)]  # Vertical line at 0x

F_code = [(0, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0),  # Vertical line at 0x
          (0, 3), (1, 3), (2, 3), (3, 3), (4, 3), (5, 3), (6, 3),  # Horizontal line at 3y
          (0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5), (0, 6)]  # Horizontal line at 0y

G_code = [(0, 3), (0, 2), (0, 4), (0.3, 1), (0.3, 5), (1.1, 0.1), (1.1, 5.9), (2.1, 0), (2.1, 6),
          (3, 0), (3, 6), (4, 0.2), (4, 5.8), (5, 1), (5, 5), (5, 4), (4, 4), (3, 4)]

H_code = [(0, 3), (1, 3), (2, 3), (3, 3), (4, 3), (5, 3), (6, 3),  # Horizontal line at 3y
          (0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5), (0, 6),  # Vertical line at 0x
          (6, 0), (6, 1), (6, 2), (6, 3), (6, 4), (6, 5), (6, 6)]  # Vertical line at 6x

I_code = [(2, 0), (4, 0), (2, 6), (4, 6),                          # Serif
          (3, 0), (3, 1), (3, 2), (3, 3), (3, 4), (3, 5), (3, 6)]  # Vertical line at 3x

J_code = [(1, 0), (2, 0), (3, 0), (4, 0),
          (4, 1), (4, 2), (4, 3), (4, 4), (4, 5), (3, 5.8),  (2, 6), (1, 6)]

K_code = [(0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5), (0, 6),  # Vertical line at 0x
          (1, 3), (2, 2), (2, 4), (3, 1), (3, 5), (4, 0), (4, 6)]

L_code = [(0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5), (0, 6),  # Vertical line at 0x
          (0, 6), (1, 6), (2, 6), (3, 6), (4, 6), (5, 6), (6, 6)]  # Horizontal line at 6y

M_code = [(0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5), (0, 6),  # Vertical line at 0x
          (6, 0), (6, 1), (6, 2), (6, 3), (6, 4), (6, 5), (6, 6),  # Vertical line at 6x
          (1, 1), (5, 1), (2, 2), (4, 2), (3, 3)]

N_code = [(0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5), (0, 6),  # Vertical line at 0x
          (6, 0), (6, 1), (6, 2), (6, 3), (6, 4), (6, 5), (6, 6),  # Vertical line at 6x
          (1, 1), (2, 2), (3, 3), (4, 4), (5, 5)]

O_code = [(0, 3), (0, 2), (0, 4), (0.3, 1), (0.3, 5), (1.1, 0.1), (1.1, 5.9), (2.1, 0), (2.1, 6),
          (3, 0), (3, 6), (3.9, 0), (3.9, 6), (4.9, 0.1), (4.9, 5.9), (5.7, 1), (5.7, 5), (6, 3), (6, 2), (6, 4)]

P_code = [(0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5), (0, 6),  # Vertical line at 0x
          (1, 0), (2, 0), (3, 0),  # Short Horizontal line at 0y
          (1, 3), (2, 3), (3, 3),  # Short Horizontal line at 3y
          (4, 1), (4, 2)]

Q_code = [(0, 3), (0, 2), (0, 4), (0.3, 1), (0.3, 5), (1.1, 0.1), (1.1, 5.9), (2.1, 0), (2.1, 6),
          (3, 0), (3, 6), (3.9, 0), (3.9, 6), (4.9, 0.1), (4.9, 5.9), (5.7, 1), (5.7, 5), (6, 3), (6, 2), (6, 4), # O
          (6, 6), (5, 5), (4, 4)]

R_code = [(0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5), (0, 6),  # Vertical line at 0x
          (1, 0), (2, 0), (3, 0),  # Short Horizontal line at 0y
          (1, 3), (2, 3), (3, 3),  # Short Horizontal line at 3y
          (4, 1), (4, 2),
          (3.3, 4), (3.6, 5), (4, 6)]

S_code = [(2, 3), (3, 3), (4, 3), (1, 2), (5, 4), (1, 1), (5, 5), (2, 0), (4, 6),
          (3, 0), (3, 6), (2, 0), (4, 6), (4, 0), (2, 6), (5, 1), (1, 5)]

T_code = [(0, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0),  # Horizontal line at 0y
          (3, 0), (3, 1), (3, 2), (3, 3), (3, 4), (3, 5), (3, 6)]  # Vertical line at 3x

U_code = [(1.9, 5.9), (3, 6), (4.1, 5.9),
          (1, 5), (1, 4), (1, 3), (1, 2), (1, 1), (1, 0),
          (5, 5), (5, 4), (5, 3), (5, 2), (5, 1), (5, 0)]

V_code = [(1, 0), (1.33, 1), (1.66, 2), (2, 3), (2.33, 4), (2.66, 5), (3, 6),
          (5, 0), (4.66, 1), (4.33, 2), (4, 3), (3.66, 4), (3.33, 5)]

W_code = [(0, 0), (0.25, 1), (0.5, 2), (0.75, 3), (1, 4), (1.25, 5), (1.5, 6),
          (6, 0), (5.75, 1), (5.5, 2), (5.25, 3), (5, 4), (4.75, 5), (4.5, 6),
          (2, 5), (2.5, 4), (3, 3),
          (4, 5), (3.5, 4)]

X_code = [(0, 0), (1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6),
          (0, 6), (1, 5), (2, 4), (3, 3), (4, 2), (5, 1), (6, 0)]

Y_code = [(0, 0), (1, 1), (2, 2), (6, 0), (5, 1), (4, 2),
          (3, 3), (3, 4), (3, 5), (3, 6)]

Z_code = [(0, 6), (1, 6), (2, 6), (3, 6), (4, 6), (5, 6), (6, 6),  # Horizontal line at 6y
          (0, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0),  # Horizontal line at 0y
          (0, 6), (1, 5), (2, 4), (3, 3), (4, 2), (5, 1), (6, 0)]

codes = {"A": A_code,
         "B": B_code,
         "C": C_code,
         "D": D_code,
         "E": E_code,
         "F": F_code,
         "G": G_code,
         "H": H_code,
         "I": I_code,
         "J": J_code,
         "K": K_code,
         "L": L_code,
         "M": M_code,
         "N": N_code,
         "O": O_code,
         "P": P_code,
         "Q": Q_code,
         "R": R_code,
         "S": S_code,
         "T": T_code,
         "U": U_code,
         "V": V_code,
         "W": W_code,
         "X": X_code,
         "Y": Y_code,
         "Z": Z_code}

def get_codes():
    return codes