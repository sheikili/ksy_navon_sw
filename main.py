#!/usr/bin/python

import sys
import random
from datetime import datetime
from enum import Enum
from statistics import mean
import time
import yaml

from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt, pyqtSlot, QBasicTimer
from PyQt5.QtWidgets import QMainWindow, QLabel, QApplication, QPushButton, QWidget, QHBoxLayout, QVBoxLayout, QStyle, \
    QDialog

from codes import get_codes

#Contatins just instructions
class MainWidget(QMainWindow):

    def __init__(self):
        super().__init__()
        config = get_config()
        self.target = config["target"]
        self.enabled = config["enabled"]
        self.not_target = [value for value in self.enabled if value not in self.target]
        self.letters_per_step = config["letters_per_step"]
        self.steps = config["steps_per_test"]
        self.time_per_step = config["time_per_step"]

        self.x_step = config["x_step"]
        self.y_step = config["y_step"]
        self.codes = get_codes()

        self.initUI()

    def initUI(self):


        cur_y = 50

        descrip_label_0_en = f'Hi! In this task you need to press the key b on your keyboard or button "B" under the task,' \
                             f'if you see sny of these letters: {self.target}\n' \
                             f'Otherwise press "n" (standing for "no") or "N" button'

        descrip_label_1_en = f'These letters may appear as a part of a bigger letter like here:'

        descrip_label_2_en = f'Or be consisted of smaller letters like here:'

        descrip_label_3_en = f'In the both cases you need to press "B". Otherwise press "N", like in the example below:'

        descrip_label_4_en = f'You will see {self.letters_per_step} such random letters for {self.steps} steps in a row.\n' \
                             f'You will have {self.time_per_step} seconds to consider if you see the target letters {self.target} ' \
                             f'in some form or not \n' \
                             f'And press a button under the letters or on your keyboard.  After the test you will obtain the statistics. \n' \
                             f'Good luck!'

        descrip_label_0 = f'V tomto zadání Vy musite tísknout tlačítko "B" na klávesnici nebo pod zadáním' \
                          f'Pokud Vy uvidíte nějaké z těchto pismen: {self.target}\n' \
                          f'Jinak tiskněte "N" (jako "ne")'

        descrip_label_1 = f'Táto pismena můžou být součastí velkých posmen jako tady:'

        descrip_label_2 = f'Nebo být složené z jiných pismen jako tady'

        descrip_label_3 = f'V obou těchto případech měli byste stísknout "B". Jinak "N", jako v podobných příkladech:'

        descrip_label_4 = f'Vy uvidíte {self.letters_per_step} nějakých složených pismen v {self.steps} kolech\n' \
                          f'A pokažde budete mít {self.time_per_step} sekund na stísknutí odpovidajicího tlačitka ' \
                          f'Po testu dostanete statistiky úspěchu\n' \
                          f'Hodně štěstí!'

        descrip_label_0 = QLabel(descrip_label_0, self)
        descrip_label_0.setGeometry(80, cur_y, 1000, 50)
        cur_y += 50

        descrip_label_1 = QLabel(descrip_label_1, self)
        descrip_label_1.setGeometry(80, cur_y, 1000, 30)
        cur_y += 30

        self.print_letter("A", self.target[0], (80, cur_y))
        cur_y += 30 + self.y_step * 7

        descrip_label_2 = QLabel(descrip_label_2, self)
        descrip_label_2.setGeometry(80, cur_y, 1000, 30)
        cur_y += 30

        self.print_letter(self.target[0], "B", (80, cur_y))
        cur_y += 30 + self.y_step * 7

        descrip_label_3 = QLabel(descrip_label_3, self)
        descrip_label_3.setGeometry(80, cur_y, 1000, 30)
        cur_y += 30

        self.print_letter(self.not_target[0], self.not_target[1], (80, cur_y))
        self.print_letter(self.not_target[2], self.not_target[3], (100 + self.x_step * 7, cur_y))
        cur_y += 30 + self.y_step * 7

        descrip_label_4 = QLabel(descrip_label_4, self)
        descrip_label_4.setGeometry(80, cur_y, 1000, 60)
        cur_y += 70

        self.cont_button = QPushButton('Continue', self)
        self.cont_button.move(120, cur_y)
        self.cont_button.clicked.connect(self.buttonWindow1_onClick)

        # cont_button.clicked.connect(QApplication.instance().quit)

        self.statusBar().showMessage('Ready')
        self.setGeometry(100, 100, 1280, 880)

        self.setWindowTitle('Navon test')
        self.show()

    @pyqtSlot()
    def buttonWindow1_onClick(self):
        self.startTest()

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Space:
            self.startTest()

    def startTest(self):
        self.statusBar().showMessage("Test started")
        self.testWindow = TestWindow()
        self.testWindow.show()
        self.close()

    def print_letter(self, big_letter, small_letter, base):
        for code in self.codes[big_letter]:
            lbl = QLabel(small_letter, self)
            lbl.move(base[0] + code[0] * self.x_step, base[1] + code[1] * self.y_step)

# Uniform widget for complex letters
class LetterWidget(QWidget):
    def __init__(self, big_letter, small_letter, x_step, y_step):
        super().__init__()
        self.setFixedSize(x_step * 7, y_step * 7)
        base = (0, 0)
        self.codes = get_codes()
        for code in self.codes[big_letter]:
            lbl = QLabel(small_letter, self)
            lbl.move(base[0] + code[0] * x_step, base[1] + code[1] * y_step)


class TestState(Enum):
    ASKED = 0
    ANSWERED = 1
    LOST = 2

# Task window
class TestWindow(QDialog):

    def __init__(self, parent=None):
        super().__init__(parent)
        config = get_config()
        self.target = config["target"]
        self.enabled = config["enabled"]
        self.not_target = [value for value in self.enabled if value not in self.target]
        self.letters_per_step = config["letters_per_step"]
        self.steps_per_test = config["steps_per_test"]
        self.time_per_step = config["time_per_step"]
        self.delay_time = config["delay_time"]

        self.x_step = config["x_step"]
        self.y_step = config["y_step"]

        self.set_UI()
        self.initTest()

    def set_UI(self):
        self.setWindowTitle('Navon test')
        self.setWindowIcon(self.style().standardIcon(QStyle.SP_FileDialogInfoView))
        self.common_layout = QVBoxLayout()

        self.letter_layout = QHBoxLayout()
        self.common_layout.addLayout(self.letter_layout)
        self.letter_widgets = []

        self.chose_buttons_layout = QHBoxLayout()
        self.b_chose_button = QPushButton(self)
        self.b_chose_button.setText('B')
        self.n_chose_button = QPushButton(self)
        self.n_chose_button.setText('N')
        self.b_chose_button.clicked.connect(self.b_chose_button_onClick)
        self.n_chose_button.clicked.connect(self.n_chose_button_onClick)
        self.chose_buttons_layout.addWidget(self.b_chose_button)
        self.chose_buttons_layout.addWidget(self.n_chose_button)
        self.common_layout.addLayout(self.chose_buttons_layout)

        self.info_label = QtWidgets.QLabel('')
        self.info_label.setAlignment(Qt.AlignHCenter)
        self.info_label.setMinimumSize(40, 20)
        self.common_layout.addWidget(self.info_label)

        # self.return_button = QPushButton(self)
        # self.return_button.setText('Return to rules')
        # self.return_button.clicked.connect(self.goMainWindow)
        # self.common_layout.addWidget(self.return_button)

        self.setLayout(self.common_layout)

    def initTest(self):
        self.time = time.time()
        self.timer = QBasicTimer()
        self.timer.start(100, self)

        self.true_present = None
        self.target_is_global = None

        self.not_target = [value for value in self.enabled if value not in self.target]

        self.step = 0
        self.global_mistakes = 0  # There was a global letter, but the user pressed N
        self.global_reactions = []  # time of reaction on global letter appearing with right guess
        self.local_mistakes = 0  # There was a local letter, but the user pressed N
        self.local_reactions = []  # time of reaction on local letter appearing with right guess
        self.absent_mistakes = 0  # There were no target letters, but the user pressed B
        self.absent_reactions = []  # time of reaction without target letter appearing with right guess
        self.lates = 0

        self.delaying = False

        self.take_step()

    # One iteration of the task
    def take_step(self):
        if self.step == self.steps_per_test:
            self.timer.stop()
            self.finish_test()
            return
        new_letters = self.get_letters()
        [self.letter_layout.removeWidget(letter) for letter in self.letter_widgets]
        [letter.setParent(None) for letter in self.letter_widgets]
        self.letter_widgets = new_letters
        [self.letter_layout.addWidget(letter) for letter in self.letter_widgets]
        self.state = TestState.ASKED
        self.set_info_label("")
        self.step += 1
        self.time = time.time()

    # Generate a lists of letters per iteration
    # Set, if there is a target one and which form it has
    def get_letters(self):
        variant = random.randint(0, 3)

        new_letters = []
        if variant == 0:  # There's a local target Letter
            self.true_present = True  # It's not nice to set here params like this, but I do
            self.target_is_global = False
            chosen_letter = random.choice(self.target)
            chosen_placement = random.randint(0, self.letters_per_step - 1)
            for i in range(self.letters_per_step):
                if i == chosen_placement:
                    new_letters.append(LetterWidget(random.choice(self.not_target), chosen_letter,
                                                    self.x_step, self.y_step))
                else:
                    new_letters.append(LetterWidget(random.choice(self.not_target), random.choice(self.not_target),
                                                    self.x_step, self.y_step))

        elif variant == 1:  # There's a Global target Letter
            self.true_present = True
            self.target_is_global = True
            chosen_letter = random.choice(self.target)
            chosen_placement = random.randint(0, self.letters_per_step - 1)
            for i in range(self.letters_per_step):
                if i == chosen_placement:
                    new_letters.append(LetterWidget(chosen_letter, random.choice(self.not_target),
                                                    self.x_step, self.y_step))
                else:
                    new_letters.append(LetterWidget(random.choice(self.not_target), random.choice(self.not_target),
                                                    self.x_step, self.y_step))
        else:  # There's not a target Letter
            self.true_present = False
            self.target_is_global = None
            for i in range(self.letters_per_step):
                new_letters.append(LetterWidget(random.choice(self.not_target), random.choice(self.not_target),
                                                self.x_step, self.y_step))
        return new_letters

    @pyqtSlot()
    def n_chose_button_onClick(self):
        self.guess_time = time.time()
        self.check_answer(False)

    @pyqtSlot()
    def b_chose_button_onClick(self):
        self.guess_time = time.time()
        self.check_answer(True)

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_B:
            self.guess_time = time.time()
            self.check_answer(True)
        elif e.key() == Qt.Key_N:
            self.guess_time = time.time()
            self.check_answer(False)

    def check_answer(self, answer):
        if self.state in [TestState.LOST, TestState.ANSWERED]:
            return
        reaction_time = self.guess_time - self.time
        self.state = TestState.ANSWERED

        if answer is self.true_present:
            self.set_info_label("Right!")
            if not self.true_present:
                self.absent_reactions.append(reaction_time)
            elif self.target_is_global is True:
                self.global_reactions.append(reaction_time)
            else:
                self.local_reactions.append(reaction_time)

        else:
            self.set_info_label("Mistake :)")
            if not self.true_present:
                self.absent_mistakes += 1
            elif self.target_is_global is True:
                self.global_mistakes += 1
            else:
                self.local_mistakes += 1

    def set_info_label(self, text):
        self.info_label.setText(text)
        # print(text)

    def goMainWindow(self):
        self.cams = MainWidget()
        self.cams.show()
        self.close()

    # Runs in a backgroud, checks time-related restrictions/events
    def timerEvent(self, event):
        """handles timer event"""
        if event.timerId() == self.timer.timerId():
            if (self.delaying and time.time() - self.finish_time >= self.delay_time):
                self.delaying = False
                self.take_step()
            if (time.time() - self.time > self.time_per_step and self.state == TestState.ASKED):
                self.state = TestState.LOST
                self.lates += 1
                self.set_info_label("Too slow : )")
                self.delaying = True
                self.finish_time = time.time()
            if (self.state == TestState.ANSWERED and self.delaying is False):
                self.delaying = True
                self.finish_time = time.time()

        else:
            super(LetterWidget, self).timerEvent(event)

    # Gathers results, opens the results window
    def finish_test(self):
        results = {}
        results["global_mistakes"] = self.global_mistakes
        results["local_mistakes"] = self.local_mistakes
        results["absent_mistakes"] = self.absent_mistakes
        if len(self.global_reactions) > 0:
            results["global_reactions"] = "%.3f" % mean(self.global_reactions)
        else:
            results["global_reactions"] = "NaN"

        if len(self.local_reactions) > 0:
            results["local_reactions"] = "%.3f" % mean(self.local_reactions)
        else:
            results["local_reactions"] = "NaN"

        if len(self.absent_reactions) > 0:
            results["absent_reactions"] = "%.3f" % mean(self.absent_reactions)
        else:
            results["absent_reactions"] = "NaN"

        results["lates"] = self.lates
        self.results_window = ResultsWindow(results)
        self.results_window.show()
        self.close()

class ResultsWindow(QDialog):
    def __init__(self, results, parent=None):
        super().__init__(parent)
        self.set_UI(results)

    def set_UI(self, results):
        self.setWindowTitle('Navon test result')
        self.common_layout = QVBoxLayout()
        def append_layout_with_label(text, layout=self.common_layout):
            layout.addWidget(QtWidgets.QLabel(text))
        lines = ["Výsledky testu:",
                 "Reakční časy u spravných voleb:",
                 f"Cílové pismeno je složené (globální uroveň): {results['global_reactions']} s",
                 f"Cílové pismeno je součastí složeného (lokální uroveň): {results['local_reactions']} s",
                 f"Cílové pismeno nebylo součastí daných pismen: {results['absent_reactions']} s",
                 "Chyby:",
                 f"Cílové pismeno je složené a nebylo nalezené (globální uroveň): {results['global_mistakes']}",
                 f"Cílové pismeno je součastí složeného a nebylo nalezené (lokální uroveň): {results['local_mistakes']}",
                 f"Cílové pismeno je chybně označeno jako nalezené: {results['absent_mistakes']}",
                 f"Zpoždění: {results['lates']}"]
        for line in lines:
            append_layout_with_label(line)
        self.setLayout(self.common_layout)

        file = open('log.txt', 'a+')
        file.write(f'{datetime.now().strftime("%c")}\n')
        for line in lines:
            file.write(f"{line}\n")
        file.write("\n")
        file.close()


def main():
    app = QApplication(sys.argv)
    ex = MainWidget()
    sys.exit(app.exec_())

def get_config():
    stream = open("config.yaml", 'r')
    dict = yaml.load(stream)
    return dict

if __name__ == '__main__':
    main()
